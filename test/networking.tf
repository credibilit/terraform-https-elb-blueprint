module "vpc" {
  source = "git::ssh://git@bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.0.3"

  account = "${var.account}"
  name = "${var.name}"
  domain_name = "${var.name}.local"
  cidr_block = "${var.vpc_cidr}"
  azs = "${data.aws_availability_zones.azs.names}"
  az_count = 2
  hosted_zone_comment = "An internal hosted zone for testing"
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24"
  ]
}

resource "aws_security_group" "ssh" {
  name = "ssh-${var.name}"
  description = "Allow all ssh traffic"
  vpc_id = "${module.vpc.vpc}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "instance_http" {
  name = "instance-${var.name}"
  description = "Allow traffic from ELB"
  vpc_id = "${module.vpc.vpc}"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "elb_http_access" {
  type = "ingress"
  to_port = "${var.http_port}"
  from_port = "${var.http_port}"
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${module.elb_test.elb_security_group}"
}

resource "aws_security_group_rule" "elb_https_access" {
  type = "ingress"
  to_port = "${var.https_port}"
  from_port = "${var.https_port}"
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${module.elb_test.elb_security_group}"
}

resource "aws_iam_server_certificate" "acme" {
  name             = "${var.name}"
  certificate_body = "${file("keys/acme.com.crt")}"
  private_key      = "${file("keys/acme.com.key")}"
}
