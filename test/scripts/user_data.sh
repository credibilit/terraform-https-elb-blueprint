#!/bin/bash

apt update
apt install nginx ssl-cert -y

cat > /etc/nginx/sites-available/default <<EOF
${nginx_config}
EOF

systemctl restart nginx.service
