data "aws_region" "current" {
  current = true
}

// Get the AZs from Amazon
data "aws_availability_zones" "azs" { }

/* AWS account
 * Declare this on TF_VAR_account environment variable with your AWS account id
 * for the test account
 */
variable account {}

variable "name" {
  default = "elb-acme"
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "http_port" {
  default = "80"
}

variable "https_port" {
  default = "443"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name = "name"
    values = [ "ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*" ]
  }

  owners = [
    "099720109477"
  ]
}
