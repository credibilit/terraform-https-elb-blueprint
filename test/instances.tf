resource "aws_key_pair" "seed" {
  key_name = "${var.name}-seed"
  public_key = "${file("keys/sample-keys-do-not-use-in-production.pub")}"
}


data "template_file" "user_data" {
  template = "${file("scripts/user_data.sh")}"

  vars {
    nginx_config = "${file("nginx/default")}"
  }
}

resource "aws_instance" "web_servers" {
  ami = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.nano"
  disable_api_termination = false
  key_name = "${aws_key_pair.seed.id}"
  vpc_security_group_ids = [
    "${aws_security_group.ssh.id}",
    "${aws_security_group.instance_http.id}"
  ]
  associate_public_ip_address = true
  subnet_id = "${module.vpc.public_subnets[count.index]}"
  user_data = "${data.template_file.user_data.rendered}"

  tags {
      Name = "web-server-${var.name}-${count.index}"
  }

  count = 2
}
