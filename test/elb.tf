module "elb_test" {
  source = "../"

  account = "${var.account}"
  name = "${var.name}"
  instance_security_group = "${aws_security_group.instance_http.id}"
  subnets = ["${module.vpc.public_subnets}"]
  s3_bucket_force_destroy = true
  internal = false
  tags = "${map("Environment", "test")}"
  ssl_certificate_arn = "${aws_iam_server_certificate.acme.arn}"
}

resource "aws_elb_attachment" "https" {
  elb      = "${module.elb_test.id}"
  instance = "${element(aws_instance.web_servers.*.id, count.index)}"

  count = 2
}
