data "aws_subnet" "subnet" {
  id = "${var.subnets[0]}"
}

resource "aws_security_group" "elb" {
  name        = "${var.name}"
  description = "${var.name} HTTPS/HTTP access"
  vpc_id      = "${data.aws_subnet.subnet.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "elb_instance_http_access" {
  type = "ingress"
  to_port = "${var.instance_http_port}"
  from_port = "${var.instance_http_port}"
  protocol = "tcp"
  source_security_group_id = "${aws_security_group.elb.id}"

  security_group_id = "${var.instance_security_group}"
}

resource "aws_security_group_rule" "elb_https_access" {
  type = "ingress"
  to_port = "${var.instance_https_port}"
  from_port = "${var.instance_https_port}"
  protocol = "tcp"
  source_security_group_id = "${aws_security_group.elb.id}"

  security_group_id = "${var.instance_security_group}"
}
